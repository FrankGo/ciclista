<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }


//EJERCICIO 1 DE CONSULTAS
    
    //Consulta Nº1
    
    //Utilizando el método DAO

    public function actionConsulta1(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(DISTINCT edad) FROM ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT edad FROM ciclista',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta Nº1 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos).",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta1a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->select("edad")-> distinct(),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta Nº1 con ORM",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos).",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
    }
    //Consulta Nº2
    
    //Utilizando el método DAO

    public function actionConsulta2(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(DISTINCT edad) FROM ciclista WHERE nomequipo="Artiach"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo="Artiach"',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta Nº2 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas del equipo Artiach.",
            "sql"=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo="Artiach";',
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta2a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->select("edad")->distinct()->where('nomequipo="Artiach"'),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta Nº2 con ORM",
            "enunciado"=>"Listar las edades de los ciclistas del equipo Artiach.",
            "sql"=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo="Artiach";',
        ]);
    }
    
    //Consulta Nº3
    
    //Utilizando el método DAO

    public function actionConsulta3(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(DISTINCT edad) FROM ciclista WHERE nomequipo="Artiach" OR nomequipo="Amore Vita"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo="Artiach" OR nomequipo="Amore Vita";',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta Nº3 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas del equipo Artiach o de Amore Vita.",
            "sql"=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo="Artiach" OR nomequipo="Amore Vita"',
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta3a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->select("edad")->distinct() ->where ('nomequipo="Artiach" OR nomequipo="Amore Vita"'),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta Nº3 con ORM",
            "enunciado"=>"Listar las edades de los ciclistas del equipo Artiach o de Amore Vita.",
            "sql"=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo="Artiach" OR nomequipo="Amore Vita"',
        ]);
    }
    
    //Consulta Nº4
    
    //Utilizando el método DAO

    public function actionConsulta4(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(DISTINCT dorsal)FROM ciclista WHERE edad <25 OR edad >30')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT dorsal FROM ciclista WHERE edad <25 OR edad >30',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta Nº4 con DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30.",
            "sql"=>'SELECT DISTINCT dorsal FROM ciclista WHERE edad <25 OR edad >30',
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta4a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find()->select("dorsal")->distinct() ->where('edad <25 OR edad >30'),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta Nº4 con ORM",
            "enunciado"=>"Listar las edades de los ciclistas cuya edad sea menor que 25 o mayor que 30.",
            "sql"=>'SELECT DISTINCT dorsal FROM ciclista WHERE edad <25 OR edad >30;',
        ]);
    }
    
    //Consulta Nº5
    
    //Utilizando el método DAO

    public function actionConsulta5(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(DISTINCT dorsal) FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo="Banesto"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo="Banesto"',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta Nº5 con DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto.",
            "sql"=>'SELECT DISTINCT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo="Banesto";',
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta5a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find() ->select("dorsal") ->distinct() ->where('edad BETWEEN 28 AND 32 AND nomequipo="Banesto"'),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta Nº5 con ORM",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto.",
            "sql"=>'SELECT DISTINCT edad FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo="Banesto"',
        ]);
    }
    
    //Consulta Nº6
    
    //Utilizando el método DAO

    public function actionConsulta6(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(DISTINCT nombre) FROM ciclista WHERE CHAR_LENGTH(nombre) > 8')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre) > 8',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta Nº6 con DAO",
            "enunciado"=>"Indicar el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8.",
            "sql"=>'SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre) > 8;',
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta6a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find() ->select("nombre") ->distinct() ->where("CHAR_LENGTH(nombre) > 8"),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta Nº6 con ORM",
            "enunciado"=>"Indicar el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8.",
            "sql"=>'SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre) > 8;',
        ]);
    }
    
        //Consulta Nº7
    
    //Utilizando el método DAO

    public function actionConsulta7(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) FROM ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT UPPER(nombre) nombre, dorsal FROM ciclista',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','dorsal'],
            "titulo"=>"Consulta Nº7 con DAO",
            "enunciado"=>"Listar el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo (nombre mayúsculas) que muestre el nombre en mayúsculas.",
            "sql"=>'SELECT DISTINCT UPPER(nombre) nombre, dorsal FROM ciclista',
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta7a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Ciclista::find() ->select("UPPER(nombre) nombre,dorsal") ->distinct(),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','dorsal'],
            "titulo"=>"Consulta Nº7 con ORM",
            "enunciado"=>"Listar el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo (nombre mayúsculas) que muestre el nombre en mayúsculas.",
            "sql"=>'SELECT DISTINCT UPPER(nombre) nombre, dorsal FROM ciclista;',
        ]);
    }
    
            //Consulta Nº8
    
    //Utilizando el método DAO

    public function actionConsulta8(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(DISTINCT dorsal) FROM lleva WHERE código = "MGE"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT dorsal FROM lleva WHERE código = "MGE"',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta Nº8 con DAO",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa.",
            "sql"=>'SELECT DISTINCT dorsal FROM lleva WHERE código = "MGE";',
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta8a(){
            $dataProvider = new ActiveDataProvider([
                'query'=> Lleva::find() ->select('dorsal') ->distinct() ->where('código="MGE"'),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta Nº8 con ORM",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa.",
            "sql"=>'SELECT DISTINCT dorsal FROM lleva WHERE código="MGE";',
        ]);
    }
    
            //Consulta Nº9
    
    //Utilizando el método DAO

    public function actionConsulta9(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(DISTINCT nompuerto) FROM puerto WHERE altura > 1500')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT nompuerto FROM puerto WHERE altura > 1500',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta Nº9 con DAO",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500.",
            "sql"=>'SELECT DISTINCT nompuerto FROM puerto WHERE altura > 1500;',
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta9a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Puerto::find() ->select('nompuerto') ->distinct() ->where('altura > 1500'),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta Nº9 con ORM",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500.",
            "sql"=>'SELECT nompuerto FROM puerto WHERE altura > 1500;',
        ]);
    }
    
            //Consulta Nº10
    
    //Utilizando el método DAO

    public function actionConsulta10(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(DISTINCT dorsal) FROM puerto WHERE altura BETWEEN 1800 AND 3000 OR pendiente > 8')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT dorsal FROM puerto WHERE altura BETWEEN 1800 AND 3000 OR pendiente > 8',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta Nº10 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000.",
            "sql"=>'SELECT DISTINCT dorsal FROM puerto WHERE altura BETWEEN 1800 AND 3000 OR pendiente > 8;', 
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta10a(){
            $dataProvider = new ActiveDataProvider([
                'query'=> Puerto::find() ->select("dorsal") ->distinct() ->where("altura BETWEEN 1800 AND 3000") ->orWhere("pendiente > 8"),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta Nº10 con ORM",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000.",
            "sql"=>'SELECT DISTINCT dorsal FROM puerto WHERE altura BETWEEN 1800 AND 3000 OR pendiente > 8;',
        ]);
    }
    
            //Consulta Nº11
    
    //Utilizando el método DAO

    public function actionConsulta11(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(DISTINCT dorsal) FROM puerto WHERE altura BETWEEN 1800 AND 3000 AND pendiente > 8')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT dorsal FROM puerto WHERE altura BETWEEN 1800 AND 3000 AND pendiente > 8',
            'totalCount'=>$numero,
            'pagination'=>['pagesize'=>5,]
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta Nº11 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000.",
            "sql"=>'SELECT DISTINCT dorsal FROM puerto WHERE altura BETWEEN 1800 AND 3000 AND pendiente > 8;',
        ]);
    }
    
    //Metodo ORM (Active Record)
    
    public function actionConsulta11a(){
            $dataProvider = new ActiveDataProvider([
                'query'=>Puerto::find() ->select("dorsal") ->distinct() ->where("altura BETWEEN 1800 AND 3000") ->andWhere("pendiente > 8"),
                'pagination'=>['pagesize'=>5,]
            ]);
    
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta Nº11 con ORM",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000.",
            "sql"=>'SELECT DISTINCT dorsal FROM puerto WHERE altura BETWEEN 1800 AND 3000 AND pendiente > 8;',
        ]);
    }
    
    }