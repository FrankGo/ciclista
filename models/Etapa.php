<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Etapa".
 *
 * @property int $numetapa
 * @property int $kms
 * @property string $salida
 * @property string $llegada
 * @property int|null $dorsal
 */
class Etapa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Etapa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numetapa', 'kms', 'salida', 'llegada'], 'required'],
            [['numetapa', 'kms', 'dorsal'], 'integer'],
            [['salida', 'llegada'], 'string', 'max' => 35],
            [['numetapa'], 'unique'],
            [['dorsal'], 'exist', 'skipOnError' => true, 'targetClass' => Ciclista::className(), 'targetAttribute' => ['dorsal' => 'dorsal']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'numetapa' => 'Numetapa',
            'kms' => 'Kms',
            'salida' => 'Salida',
            'llegada' => 'Llegada',
            'dorsal' => 'Dorsal',
        ];
    }
}
