<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Puerto".
 *
 * @property string $nompuerto
 * @property int $altura
 * @property string $categoria
 * @property float|null $pendiente
 * @property int $numetapa
 * @property int|null $dorsal
 */
class Puerto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Puerto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nompuerto', 'altura', 'categoria', 'numetapa'], 'required'],
            [['altura', 'numetapa', 'dorsal'], 'integer'],
            [['pendiente'], 'number'],
            [['nompuerto'], 'string', 'max' => 35],
            [['categoria'], 'string', 'max' => 1],
            [['nompuerto'], 'unique'],
            [['dorsal'], 'exist', 'skipOnError' => true, 'targetClass' => Ciclista::className(), 'targetAttribute' => ['dorsal' => 'dorsal']],
            [['numetapa'], 'exist', 'skipOnError' => true, 'targetClass' => Etapa::className(), 'targetAttribute' => ['numetapa' => 'numetapa']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nompuerto' => 'Nompuerto',
            'altura' => 'Altura',
            'categoria' => 'Categoria',
            'pendiente' => 'Pendiente',
            'numetapa' => 'Numetapa',
            'dorsal' => 'Dorsal',
        ];
    }
}
