<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class= "jumbotron">
    <h1><b>Consutas de Selección Ciclistas<b/></h1>
        <h2><i>Ejercicio Nº1</i></h2> <br>
    </div>
    
  <div class="d-flex justify-content-center">
   <div class="row">
    <div class="col-sm-4">
        <div class="thumbnail">
        <h2>Consulta Nº1</h2>
        <p>Listar las edades de los ciclistas (sin repetidos).</p>
      <?= Html::a('Active Record', ['/site/consulta1a'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', ['/site/consulta1'], ['class' => 'btn btn-default']) ?> <br><br><br>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="thumbnail">
        <h2>Consulta Nº2</h2>
        <p>Listar las edades de los ciclistas de Artiach.</p>
      <?= Html::a('Active Record', ['/site/consulta2a'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', ['/site/consulta2'], ['class' => 'btn btn-default']) ?> <br><br><br>
      </div>
    </div>
    <div class="col-sm-4">
        <div class="thumbnail">
        <h2>Consulta Nº3</h2>
        <p>Listar las edades de los ciclistas de Artiach o de Amore Vita.</p>
            <?= Html::a('Active Record', ['/site/consulta3a'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('DAO', ['/site/consulta3'], ['class' => 'btn btn-default']) ?> <br><br><br>
        </div>
    </div>
      <div class="col-sm-4">
        <div class="thumbnail">
        <h2>Consulta Nº4</h2>
        <p>Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30.</p>
            <?= Html::a('Active Record', ['/site/consulta4a'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('DAO', ['/site/consulta4'], ['class' => 'btn btn-default']) ?> <br><br><br>
        </div>
    </div>
     <div class="col-sm-4">
        <div class="thumbnail">
        <h2>Consulta Nº5</h2>
        <p>Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto.</p>
      <?= Html::a('Active Record', ['/site/consulta5a'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', ['/site/consulta5'], ['class' => 'btn btn-default']) ?> <br><br><br>
      </div>
    </div>
     <div class="col-sm-4">
        <div class="thumbnail">
        <h2>Consulta Nª6</h2>
        <p>Indicar el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8.</p>
      <?= Html::a('Active Record', ['/site/consulta6a'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', ['/site/consulta6'], ['class' => 'btn btn-default']) ?> <br><br><br>
      </div>
    </div>
     <div class="col-sm-4">
        <div class="thumbnail">
        <h2>Consulta Nº7</h2>
        <p>Listar el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo (nombre mayúsculas) que muestre el nombre en mayúsculas.</p>
      <?= Html::a('Active Record', ['/site/consulta7a'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', ['/site/consulta7'], ['class' => 'btn btn-default']) ?> <br><br><br>
      </div>
    </div>
      <div class="col-sm-4">
       <div class="thumbnail">
        <h2>Consulta Nº8</h2>
        <p>Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa.</p>
      <?= Html::a('Active Record', ['/site/consulta8a'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', ['/site/consulta8'], ['class' => 'btn btn-default']) ?> <br><br><br>
      </div>
      </div>
     <div class="col-sm-4">
       <div class="thumbnail">
        <h2>Consulta Nº9</h2>
        <p>Listar el nombre de los puertos cuya altura sea mayor de 1500.</p>
      <?= Html::a('Active Record', ['/site/consulta9a'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', ['/site/consulta9'], ['class' => 'btn btn-default']) ?> <br><br><br>
      </div>
    </div>
     <div class="col-sm-4">
       <div class="thumbnail">
        <h2>Consulta Nº10</h2>
        <p>Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000.</p>
      <?= Html::a('Active Record', ['/site/consulta10a'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', ['/site/consulta10'], ['class' => 'btn btn-default']) ?> <br><br><br>
      </div>
    </div>
    <div class="col-sm-4">
       <div class="thumbnail">
        <h2>Consulta Nº11</h2>
        <p>Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000.</p>
      <?= Html::a('Active Record', ['/site/consulta11a'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('DAO', ['/site/consulta11'], ['class' => 'btn btn-default']) ?> <br><br><br>
      </div>
    </div>
    </div>
</div>